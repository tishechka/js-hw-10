document.addEventListener("DOMContentLoaded", function () {
    const tabTitles = document.querySelectorAll(".tabs-title");
    const tabContents = document.querySelectorAll(".tabs-content li");

    tabTitles.forEach((title, index) => {
        title.addEventListener("click", () => {

            tabContents.forEach(content => content.style.display = "none");


            tabContents[index].style.display = "block";


            tabTitles.forEach(t => t.classList.remove("active"));


            title.classList.add("active");
        });
    });
});
